<?php

namespace Drupal\robots_dtap\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class SettingsForm.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'robots_dtap.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'settings_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('robots_dtap.settings');
    $form['production_domain'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Production domain'),
      '#description' => $this->t('Fill in your final production domain(s) name (separate wth new line)'),
      '#default_value' => $config->get('production_domain'),
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $this->config('robots_dtap.settings')
      ->set('production_domain', $form_state->getValue('production_domain'))
      ->save();
  }

}

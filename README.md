CONTENTS OF THIS FILE
---------------------

* Introduction
* Requirements
* Installation
* Configuration
* Maintainers


INTRODUCTION
------------

This module adds a metatag for robots noindex, nofollow for all environments
(like staging, acceptation) you want to skip from Google (and other crawlers).

* For a full description of the module, visit the project page:
https://www.drupal.org/project/robots_dtap

* To submit bug reports and feature suggestions, or to track changes:
https://www.drupal.org/project/issues/robots_dtap


REQUIREMENTS
------------

This module requires no modules outside of Drupal core.


INSTALLATION
------------

* Install the Robots DTAP module as you would normally install a contributed
Drupal module.
Visit https://www.drupal.org/node/1897420 for further information.


CONFIGURATION
-------------

1. Navigate to Administration > Extend and enable the module.
2. Navigate to Administration > Configuration > System > DTAP settings.
3. Add your production url's (which you want to hide) in an admin setting
form. Save configuration.


MAINTAINERS
-----------

* Kees Cornelisse (keescornelisse) - https://www.drupal.org/u/keescornelisse

Supporting organizations:

* EF2 B.V. - https://www.drupal.org/ef2-bv
